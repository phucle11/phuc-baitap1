let nextTodoId = 0;
let nextCharacterId = 0;
export const addTodo = (text) => ({
    type: 'ADD_TODO',
    id: nextTodoId++,
    text
})

export const toggleTodo = (id) => ({
    type: 'TOGGLE_TODO',
    id
})

export const setText = (text) => ({
    type: 'SET_TEXT',
    text
})

export const loginRequest = (username, password) => ({
    type: 'LOGIN_REQUEST',
    username,
    password
})

export const loading = () => ({
    type: 'LOADING'
})

export const loginSuccess = () => ({
    type: 'LOGIN_SUCCESS'
})

export const logout = () => ({
    type: 'LOGOUT'
})

export const loginFail = () => ({
    type: 'LOGIN_FAIL'
})

export const getCharacter = (character) => ({
    type: 'GET_CHARACTER',
    id: nextCharacterId++,
    character
})

export const fetchRequest = (index) => ({
    type: 'FETCH_REQUEST',
    index
})

export const getImg = (images) => ({
    type: 'GET_IMG',
    images
})

export const imgRequest = () => ({
    type: 'IMG_REQUEST'
})

export const generateText = (topText, bottomText) => ({
    type: 'GENERATE_TEXT',
    topText,
    bottomText
})

export const changeMeme = (image) => ({
    type: 'CHANGE_MEME',
    image
})