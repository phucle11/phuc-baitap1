import { loginSuccess, loading, loginFail, getCharacter, getImg } from '../actions'
import { takeLatest, takeEvery, put, delay, call } from 'redux-saga/effects'
import { fetchData, fetchExtraData } from '../../api/api'
function* loginSaga(action) {
    const username = action.username
    const password = action.password
    yield put(loading())
    yield delay(200)
    username === 'phuc' && password === 'phuc'? yield put(loginSuccess()) : yield put(loginFail())
}

function* fetchSaga(action) {
    try{
        yield put(loading())
        const data = yield call(fetchData,action.index)
        const extraData = yield call(fetchExtraData,data.species)
        data.species = extraData
        yield put(getCharacter(data))
    }
    catch(e) {
        console.log(e)
    }
}

function* getImgSaga() {
    try{
        const dataImg = yield call(fetchExtraData,"https://api.imgflip.com/get_memes")
        yield put(getImg(dataImg.data.memes)) 
    }
    catch(e) {

    }
}

export function* mySaga() {
    yield takeEvery('LOGIN_REQUEST',loginSaga)
    yield takeLatest('FETCH_REQUEST',fetchSaga)
    yield takeLatest('IMG_REQUEST',getImgSaga)
}