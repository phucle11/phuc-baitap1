const images = (state = [], action) => {
    switch(action.type) {
        case 'GET_IMG':
            return [
                ...state,
                ...action.images
            ]
        default:
            return state
    }
}

export default images