const info = (state = {}, action) => {
    switch(action.type) {
        case 'LOGIN_REQUEST':
            return {
                ...state, 
                username: action.username,
                password: action.password
            }
        case 'LOGIN_FAIL':
            return {
                ...state,
                name: '',
                password: ''
            }
        case 'LOGOUT':
            return {
                ...state,
                name: '',
                password: ''
            }
        default:
            return state
    }
}

export default info;