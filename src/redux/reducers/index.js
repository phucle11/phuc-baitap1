import { combineReducers } from 'redux'
import todos from './todos'
import display from './display'
import info from './info'
import status from './status'
import characters from './characters'
import images from './images'
import meme from './meme'
import memeText from './memeText'

export default combineReducers({
    todos,
    display,
    info,
    status,
    characters,
    images,
    meme,
    memeText
})
