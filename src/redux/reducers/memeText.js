const memeText = (state = {}, action) => {
    switch(action.type) {
        case 'GENERATE_TEXT':
            return {
                ...state,
                topText: action.topText,
                bottomText: action.bottomText
            }
        default:
            return state
    }
}

export default memeText