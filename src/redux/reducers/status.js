const status = (state = {}, action) => {
    switch(action.type){
        case 'LOGIN_SUCCESS':
            console.log('login success')
            return {
                ...state,
                isAuth: true,
                logout: false,
                loading: false
            }
        case 'LOGOUT':
            return {
                ...state,
                isAuth:false,
                logout: true
            }
        case 'LOADING':
            return {
                ...state,
                loading: true
            }
        case 'LOGIN_FAIL':
            console.log('login fail')
            return {
                ...state,
                loading: false
            }
        case 'GET_CHARACTER':
            return {
                ...state,
                loading: false
            }
        case 'GET_IMG':
            return {
                ...state,
                loading: false
            }
        default:
            return state
    }
}

export default status;