const meme = (state={}, action) => {
    switch(action.type) {
        case 'CHANGE_MEME':
            return {
                ...state,
                url: action.image
            }
        default:
            return state
    }
}

export default meme 