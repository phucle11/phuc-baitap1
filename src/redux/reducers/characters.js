const characters = (state = [], action) => {
    switch(action.type) {
        case 'GET_CHARACTER':
            return [
                ...state,
                {
                    id: action.id,
                    character: action.character
                }
            ]
        default:
            return state
    }
}

export default characters