import { loginSuccess, loading, login, loginFail } from '../actions'


export const loginRequest = (name) => (dispatch) => (
    dispatch(loading()),
    setTimeout(() => (
        dispatch(login(name)),
        name === 'phuc'? dispatch(loginSuccess()): dispatch(loginFail())
    ), 1000)
)