export async function fetchData(index) {
    try {   
        if(index >= 2) index++
        const response = await fetch(`https://swapi.co/api/people/${index}`)
        console.log(response)
        const data = await response.json()
        return data
    }
    catch(e) {
        console.log(e)
    }
}

export async function fetchExtraData(path) {
    try {
        const response = await fetch(path)
        const data = await response.json()
        return data
    }
    catch(e) {
        console.log(e)
    }
}