import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './components/App'
import App2 from './components/App2'
import App3 from './components/App3'
import AppLogin from './components/AppLogin'
import AppHome from './components/AppHome'
import AppMeme from './components/AppMeme'
import NavBar from './containers/NavBar'
import * as serviceWorker from './serviceWorker'
import PrivateRoute from './helpers/PrivateRoute'

import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from './redux/reducers'
//import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga'
import { mySaga } from './redux/sagas/mySaga'

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

const sagaMiddleware = createSagaMiddleware() 

//const store = createStore(rootReducer, applyMiddleware(thunk));
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))
sagaMiddleware.run(mySaga)

ReactDOM.render(
    <Router>
        <Provider store={store}>
            <NavBar />
            <Switch>
                <Route path="/" exact component={AppHome}/>
                <PrivateRoute path="/display/" exact component={App2} />
                <Route path="/login/" exact component={AppLogin} />
                <PrivateRoute path="/todo/" exact component={App} />
                <PrivateRoute path="/characters/" exact component={App3} />
                <PrivateRoute path="/meme/" exact component={AppMeme} />
            </Switch>
        </Provider>
    </Router>
    , document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
