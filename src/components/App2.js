import React, { Fragment } from 'react'
import Input from '../containers/Input'
import Output from '../containers/Output'

const App2 = () => (
    <Fragment>
        <Input />
        <Output />
    </Fragment>
)

export default App2