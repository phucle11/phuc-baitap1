import React, { Fragment } from 'react';

const Display = ({text}) => (
        <Fragment>
            <span>OUTPUT:</span>
            <span>{text}</span>
        </Fragment>
    )
export default Display;