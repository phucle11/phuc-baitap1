import React, { Fragment } from 'react'
import GetCharacter from '../containers/GetCharacter'
import CharacterList from '../containers/CharacterList'

const App3 = () => (
    <Fragment>
        <GetCharacter />
        <CharacterList />
    </Fragment>
)

export default App3