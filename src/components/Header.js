import React, { Fragment } from 'react'
import headerImg from '../assets/Trollface.png'
import '../App.css'

const Header = () => (
    <Fragment>
        <header>
            <img src={headerImg} alt="trollface"/>
            <p>Meme Generator</p>
        </header>
    </Fragment>
)

export default Header