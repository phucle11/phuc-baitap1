import React, { Fragment } from 'react'

const Meme = ({imgUrl, topText, bottomText}) => (
    <Fragment>
        <p>{topText}</p>
        <img src={imgUrl} />
        <p>{bottomText}</p>
    </Fragment>
)

export default Meme