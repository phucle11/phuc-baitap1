import React, { Fragment } from 'react'
import Header from './Header'
import MemeGenerator from '../containers/MemeGenerator'
import { connect } from 'react-redux'
import { imgRequest } from '../redux/actions'

const AppMeme = ({dispatch}) => {
    dispatch(imgRequest())
    return (
        <Fragment>
            <Header />
            <MemeGenerator />
        </Fragment>
    )
}  

export default connect()(AppMeme)
