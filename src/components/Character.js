import React from 'react'

const Character =  ({character}) => (
    <li>
        <p>name: {character.name}</p>
        <p>gender: {character.gender}</p>
        <p>species: {character.species.name}</p>
    </li>
)

export default Character;