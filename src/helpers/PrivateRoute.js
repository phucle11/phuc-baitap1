import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute = ({isAuth, component: Component, ...rest}) => (
    <Route 
        {...rest}
        render={
            props => isAuth? <Component {...props} />
            : <Redirect to="/login/" />
        }
    />
)

const mapStateToProps = (state) => ({
    isAuth: state.status.isAuth
})

export default connect(mapStateToProps)(PrivateRoute)