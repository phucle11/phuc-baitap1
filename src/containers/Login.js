import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
//import { loginRequest } from '../thunk/loginRequest';
import { loginRequest } from '../redux/actions';
import './Login.css'
import logo from '../logo.svg';

const Login = ({dispatch, isAuth, loading}) => {
    let userName
    let passWord
    return(
        <Fragment>
            <form onSubmit={e => {
                e.preventDefault();
                if(userName.value.trim() === '')
                return
                dispatch(loginRequest(userName.value, passWord.value))
                userName.value=''
                passWord.value=''
            }}>
                <input placeholder="username" ref={node => userName = node}/>
                <input placeholder="password" type="password" ref={node => passWord = node} />
                <button type="submit">Login</button>
            </form>
            <p>*username: phuc, password: phuc</p>
            {loading && <p className="loading"><img src={logo} className="App-logo" />Loading...</p>}
            {isAuth && <Redirect to="/" />}
        </Fragment>
    );
}

const mapStateToProps = (state) => ({
    isAuth: state.status.isAuth,
    loading: state.status.loading
})

export default connect(mapStateToProps)(Login)