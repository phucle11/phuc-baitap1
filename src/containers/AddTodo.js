import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { addTodo } from '../redux/actions';

const AddTodo = ({dispatch}) => {
    let input
    return(
        <Fragment>
            <form onSubmit={e => {
                e.preventDefault();
                if(input.value.trim() === '')
                return
                dispatch(addTodo(input.value))
                input.value=''    
            }}>
                <input ref={node => input = node}/>
                <button type="submit">Add</button>
            </form>
        </Fragment>
    )
}

export default connect()(AddTodo)