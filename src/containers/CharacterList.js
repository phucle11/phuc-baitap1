import React, { Fragment } from 'react';
import Character from '../components/Character'
import { connect } from 'react-redux'

import './Login.css'
import logo from '../logo.svg';

const CharacterList = ({characters, loading}) => (
    <Fragment>
        {loading && <p className="loading"><img src={logo} className="App-logo" />Loading...</p>}
        <ol>
            {characters.map(character =>
            <Character
            key={character.id}
            {...character}
            />)}
        </ol>
    </Fragment>
)

const mapStateToProps = (state) => ({
    characters: state.characters,
    loading: state.status.loading
})

export default connect(mapStateToProps)(CharacterList);