import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import Meme from '../components/Meme'
import { generateText, changeMeme } from '../redux/actions'

const MemeGenerator = ({images, url, topText, bottomText, dispatch}) => {
    let imgUrls =[]
    images.map(img => imgUrls.push(img.url) )
    let imgUrl
    return (
        <Fragment>
            <form>
                <input placeholder="top text" onChange={e=> {
                    e.preventDefault()
                    dispatch(generateText(topText.value, bottomText.value))
                }} ref={node => topText = node}/>
                <input placeholder="bottom text" onChange={e=> {
                    e.preventDefault()
                    dispatch(generateText(topText.value, bottomText.value))
                }} ref={node => bottomText = node}/>
                <button type="button" onClick={e => {
                e.preventDefault()
                imgUrl = imgUrls[Math.round(Math.random()*100)]
                dispatch(changeMeme(imgUrl))
                }}>
                    Get New Meme
                </button>
            </form>
            <Meme imgUrl={url} topText={topText} bottomText={bottomText} />
        </Fragment>
    )
}

const mapStateToProps = (state) => ({
    images: state.images,
    url: state.meme.url,
    topText: state.memeText.topText,
    bottomText: state.memeText.bottomText
})

export default connect(mapStateToProps)(MemeGenerator)