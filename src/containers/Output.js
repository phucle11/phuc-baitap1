import { connect } from 'react-redux';
import Display from '../components/Display';

const mapStateToProps = (state) => ({
    text: state.display.text
})

export default connect(
    mapStateToProps
)(Display)