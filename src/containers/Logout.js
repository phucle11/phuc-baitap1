import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { logout } from '../redux/actions';
import { Redirect } from 'react-router-dom';

const Logout = ({dispatch, willLogOut}) => {
    return(
        <Fragment>
            <form>
                <button onClick={(e) => {
                    e.preventDefault()
                    dispatch(logout())
                }}>Logout</button>
            </form>
            {willLogOut && <Redirect to="/" />}
        </Fragment>
    )
}

const mapStateToProps = (state) => ({
    willLogOut: state.status.logout
})

export default connect(mapStateToProps)(Logout)