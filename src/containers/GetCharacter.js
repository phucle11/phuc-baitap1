import React, { Fragment } from 'react'
import { fetchRequest } from '../redux/actions'
import { connect } from 'react-redux' 

const GetCharacter = ({dispatch, index, characters}) => {
    return (
        <Fragment>
        <button type="button" onClick={e => {
            e.preventDefault()
            dispatch(fetchRequest(index))
        }}>Get Character</button>
        
    </Fragment>
    )
}

const mapStateToProps = (state) => ({
    index: state.characters.length+1,
    characters: state.characters
})

export default connect(mapStateToProps)(GetCharacter)