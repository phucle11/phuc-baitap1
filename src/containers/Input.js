import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { setText } from '../redux/actions'

const Input = ({dispatch}) => {
    let input 
    return(
        <Fragment>
            <form>
                <label>INPUT</label>
                <input onChange={e => {
                    e.preventDefault();
                    dispatch(setText(input.value))
                }}
                 ref={node => input = node}/>
            </form>
            <p></p>
        </Fragment>
    )
}

export default connect()(Input)