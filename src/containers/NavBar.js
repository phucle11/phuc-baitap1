import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import Logout from './Logout'

const NavBar = ({isAuth}) => (
    <Fragment>
        <div className="nav">
                <button><Link to="/">Home</Link></button>
                <button><Link to="/todo/">Todo App</Link></button><br />
                <button><Link to="/display/">Display input</Link></button>
                <button><Link to="/characters/">Characters</Link></button>
                <button><Link to="/meme/">Meme</Link></button>
                {!isAuth && <button><Link to="/login/">Login</Link></button>}
                {isAuth && <Logout />}
            </div>
    </Fragment>
)

const mapStateToProps = (state) => ({
    isAuth: state.status.isAuth
})

export default connect(mapStateToProps)(NavBar)